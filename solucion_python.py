# -*- coding: utf-8 -*-

import json
import requests

URL_COMMENTS = 'https://jsonplaceholder.typicode.com/posts/'

URL_API_AEMET = 'https://opendata.aemet.es/opendata/api/prediccion/especifica/municipio/diaria/'
TOKEN_AEMET = '<< TOKEN ENTREGADO POR AEMET >>'


def api_get_call(url, headers=None):
	"""
	Peticiones GET a APIs, con encabezado opcional
	:return Respuesta de API
	"""
	response = requests.get(url, headers=headers)
	response.raise_for_status()  # Excepción por si hay error en la petición
	return json.loads(response.text)


def ejercicio_1():
	data = api_get_call(URL_COMMENTS)
	users = list(set([d['userId'] for d in data]))
	user_comments = {u: {'comments': []} for u in users}
	
	for d in data:
		user_comments[d['userId']]['comments'].append(d['body'])
		
	print("\n\nEjercicio 1:\n----------\n")
	print("Parte A:\n")
	for u in users:
		print(f"Usuario ID {u}: {len(user_comments[u]['comments'])}")
		
	print("\n-------\n")
	
	print("Parte B:")
	for u in users:
		print(f"\nUsuario ID {u}:")
		print(f"{user_comments[u]['comments'][-2:]}")


def ejercicio_2():
	headers = {
		'api_key': TOKEN_AEMET,
		'Accept': 'application/json'
	}
	
	COD_CITY = 28079
	
	endpoint = f"{URL_API_AEMET}/{COD_CITY}"
	data = api_get_call(endpoint, headers=headers)
	url_datos = data['datos']
	new_data = api_get_call(url_datos)
	
	print("\n\nEjercicio 2:\n----------\n")
	print(f"Resultado: {new_data[0]['prediccion']}")


ejercicio_1()
ejercicio_2()

